# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class AccountSmartReportsTestCase(ModuleTestCase):
    """Test Account Smart Reports module"""
    module = 'account_smart_reports'


del ModuleTestCase
