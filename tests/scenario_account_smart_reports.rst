========================
Account Reports Scenario
========================

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard, Report
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax, create_tax_code
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences
    >>> today = datetime.date.today()

Install account::

    >>> config = activate_modules('account_smart_reports')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')
    >>> period = fiscalyear.periods[0]

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> receivable = accounts['receivable']
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']
    >>> cash = accounts['cash']

Create taxes::

    >>> Tax = Model.get('account.tax')
    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()
    >>> tax_code = create_tax_code(tax)
    >>> tax_code.vat_return = 'charged'
    >>> tax_code.save()
    >>> tax_code2 = create_tax_code(tax, amount='base')
    >>> tax_code2.vat_return = 'charged'
    >>> tax_code2.save()
    >>> tax2 = create_tax(Decimal('.21'))
    >>> tax2.save()
    >>> tax_code3 = create_tax_code(tax2)
    >>> tax_code3.vat_return = 'deducted'
    >>> tax_code3.save()
    >>> tax_code4 = create_tax_code(tax2, amount='base')
    >>> tax_code4.vat_return = 'deducted'
    >>> tax_code4.save()

Create parties::

    >>> Party = Model.get('party.party')
    >>> party = Party(name='Party')
    >>> party.save()

Create account category::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.customer_taxes.append(tax)
    >>> account_category.supplier_taxes.append(tax2)
    >>> account_category.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.list_price = Decimal('40')
    >>> template.account_category = account_category
    >>> template.save()
    >>> product, = template.products

Create a moves::

    >>> Journal = Model.get('account.journal')
    >>> Move = Model.get('account.move')
    >>> journal_revenue, = Journal.find([
    ...         ('code', '=', 'REV'),
    ...         ])
    >>> journal_cash, = Journal.find([
    ...         ('code', '=', 'CASH'),
    ...         ])
    >>> move = Move()
    >>> move.period = period
    >>> move.journal = journal_revenue
    >>> move.date = period.start_date
    >>> line = move.lines.new()
    >>> line.account = revenue
    >>> line.credit = Decimal(10)
    >>> line = move.lines.new()
    >>> line.account = receivable
    >>> line.debit = Decimal(10)
    >>> line.party = party
    >>> move.click('post')

    >>> move = Move()
    >>> move.period = period
    >>> move.journal = journal_cash
    >>> move.date = period.start_date
    >>> line = move.lines.new()
    >>> line.account = cash
    >>> line.debit = Decimal(10)
    >>> line = move.lines.new()
    >>> line.account = receivable
    >>> line.credit = Decimal(10)
    >>> line.party = party
    >>> move.click('post')

Print some reports::

    >>> GeneralLedgerAccount = Model.get('account.general_ledger.account')
    >>> GeneralLedgerParty = Model.get('account.general_ledger.account.party')
    >>> gl_accounts = GeneralLedgerAccount.find([('account', 'in', [receivable, cash])])
    >>> _ = [(l.balance, l.party_required) for gl in gl_accounts
    ...     for l in gl.lines]
    >>> gl_by_parties = GeneralLedgerParty.find([])

    >>> general_ledger = Report('account.general_ledger', context={
    ...     'company': company.id,
    ...     'fiscalyear': fiscalyear.id,
    ...     })
    >>> _ = general_ledger.execute(gl_accounts)

    >>> with config.set_context(fiscalyear=fiscalyear.id,
    ...         from_date=period.start_date, to_date=period.start_date):
    ...     general_ledger_list = Report('account.general_ledger.list')
    ...     ext, content, _, name = general_ledger_list.execute(gl_accounts)
    >>> ext
    'csv'
    >>> name
    'General ledger list-Main Cash-Main Receivable'
    >>> for row in content.decode().split('\r\n'):
    ...     row.replace(period.start_date.strftime('%m/%d/%Y'), '01/01/2022')
    'Main Cash'
    'Date;Move;Description;Origin;Origin (Record Name);Debit;Credit;Balance'
    ';;Start Balance;;;;;0.00'
    '01/01/2022;2;;;;10.00;0.00;10.00'
    ';;;;Total;10.00;0.00;'
    ';;End Balance;;;;;10.00'
    ''
    'Main Receivable'
    'Date;Move;Party;Description;Origin;Origin (Record Name);Debit;Credit;Balance'
    ';;;Start Balance;;;;;0.00'
    '01/01/2022;1;Party;;;;10.00;0.00;10.00'
    '01/01/2022;2;Party;;;;0.00;10.00;0.00'
    ';;;;;Total;10.00;10.00;'
    ';;;End Balance;;;;;0.00'
    ''
    ''

    >>> with config.set_context(fiscalyear=fiscalyear.id,
    ...         from_date=period.start_date, to_date=period.start_date):
    ...     general_ledger_list = Report('account.general_ledger.list')
    ...     ext, content, _, name = general_ledger_list.execute(gl_by_parties)
    >>> ext
    'csv'
    >>> name
    'General ledger list-Main Receivable - Party'
    >>> for row in content.decode().split('\r\n'):
    ...     row.replace(period.start_date.strftime('%m/%d/%Y'), '01/01/2022')
    'Main Receivable - Party'
    'Date;Move;Description;Origin;Origin (Record Name);Debit;Credit;Balance'
    ';;Start Balance;;;;;0.00'
    '01/01/2022;1;;;;10.00;0.00;10.00'
    '01/01/2022;2;;;;0.00;10.00;0.00'
    ';;;;Total;10.00;10.00;'
    ';;End Balance;;;;;0.00'
    ''
    ''

Create invoices::

    >>> Invoice = Model.get('account.invoice')
    >>> invoice = Invoice()
    >>> invoice.type = 'in'
    >>> invoice.party = party
    >>> invoice.reference = 'Invoice 1'
    >>> invoice.invoice_date = today
    >>> line = invoice.lines.new()
    >>> line.product = product
    >>> line.quantity = 5
    >>> line.unit_price = Decimal('20')
    >>> invoice.click('post')

    >>> invoice2 = Invoice()
    >>> invoice2.type = 'in'
    >>> invoice2.party = party
    >>> invoice2.invoice_date = today
    >>> line = invoice2.lines.new()
    >>> line.product = product
    >>> line.quantity = 1
    >>> line.unit_price = Decimal('50')
    >>> invoice2.click('post')

    >>> invoice3 = Invoice()
    >>> invoice3.type = 'out'
    >>> invoice3.party = party
    >>> invoice3.invoice_date = today
    >>> invoice3.reference = 'Invoice 3'
    >>> line = invoice3.lines.new()
    >>> line.product = product
    >>> line.quantity = 5
    >>> line.unit_price = Decimal('20')
    >>> invoice3.click('post')

Vat return report without show invoice detail::

    >>> Period = Model.get('account.period')
    >>> period, = Period.find([
    ...     ('start_date', '<=', today),
    ...     ('end_date', '>=', today),
    ...     ('fiscalyear.company', '=', company),
    ...     ('type', '=', 'standard')],
    ...     order=[('start_date', 'DESC')], limit=1)
    >>> vat_return = Wizard('account.tax.vat_return.print')
    >>> vat_return.form.periods.append(period)
    >>> period = Period(period.id)
    >>> vat_return.form.type = 'both'
    >>> vat_return.form.grouping = 'tax'
    >>> vat_return.execute('print_')
    >>> ext, content, _, name = vat_return.actions[0]
    >>> ext
    'csv'
    >>> for row in content.decode().split('\r\n'):
    ...     row
    'Deducted'
    'Name;Base;Tax;Base+Tax'
    'Tax 0.21;150.00;31.50;181.50'
    'Total;150.00;31.50;181.50'
    ''
    'Charged'
    'Name;Base;Tax;Base+Tax'
    'Tax 0.10;100.00;10.00;110.00'
    'Total;100.00;10.00;110.00'
    ''
    'Difference;;21.50;'
    ''
    >>> name
    'Vat Return'

Vat return report with show invoice detail::

    >>> vat_return = Wizard('account.tax.vat_return.print')
    >>> vat_return.form.periods.append(period)
    >>> period = Period(period.id)
    >>> vat_return.form.type = 'both'
    >>> vat_return.form.grouping = 'invoice'
    >>> vat_return.execute('print_')
    >>> _, content, _, _ = vat_return.actions[0]
    >>> for row in content.decode().split('\r\n'):
    ...     row.replace(today.strftime('%Y-%m-%d'), '(date)')
    'Deducted'
    'Tax;Effective Date;Account;Number;Reference;Invoice Date;Party;Party Tax Identifier;Base;Tax;Base+Tax;Invoice amount'
    'Tax 0.21;(date);;1;Invoice 1;(date);Party;;100.00;21.00;121.00;121.00'
    'Tax 0.21;(date);;2;;(date);Party;;50.00;10.50;60.50;60.50'
    'Total;;;;;;;;150.00;31.50;181.50;'
    'Charged'
    'Tax;Effective Date;Account;Number;Reference;Invoice Date;Party;Party Tax Identifier;Base;Tax;Base+Tax;Invoice amount'
    'Tax 0.10;(date);;3;Invoice 3;(date);Party;;100.00;10.00;110.00;110.00'
    'Total;;;;;;;;100.00;10.00;110.00;'
    ''