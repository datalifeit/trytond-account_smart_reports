# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields, ModelView
from trytond.pyson import Eval, If
from trytond.wizard import Wizard, StateView, StateReport, Button
from trytond.transaction import Transaction
from trytond.tools import reduce_ids, cursor_dict
from trytond import backend
from trytond.modules.ir_report_csv.model import CSVReport
from itertools import groupby
from sql import Literal
from sql.aggregate import Sum
from sql.conditionals import Case
from sql.functions import Abs


class VatReturnMixin(object):
    __slots__ = ()

    vat_return = fields.Selection([
        (None, ''),
        ('charged', 'Charged'),
        ('deducted', 'Deducted')
        ], 'Vat return')

    @staticmethod
    def default_vat_return():
        return None


class TaxCode(VatReturnMixin, metaclass=PoolMeta):
    __name__ = 'account.tax.code'


class TaxCodeTemplate(VatReturnMixin, metaclass=PoolMeta):
    __name__ = 'account.tax.code.template'


class PrintVatReturnStart(ModelView):
    """Print Vat Return Start"""
    __name__ = 'account.tax.vat_return.print.start'

    type = fields.Selection([
            ('charged', 'Charged'),
            ('deducted', 'Deducted'),
            ('both', 'Both')
        ], 'Type', required=True)
    grouping = fields.Selection([
            ('invoice', 'Invoice'),
            ('tax', 'Tax')
        ], 'Grouping', required=True,
        domain=[If(Eval('type') == 'both',
            ('grouping', '=', 'tax'),
            ())
        ])
    ordering = fields.Selection([
            (None, ''),
            ('invoice', 'Invoice'),
            ('date', 'Accounting Date')
        ], 'Ordering',
        domain=[If((Eval('grouping') == 'invoice'),
            ('ordering', '!=', None),
            ())],
        states={
            'invisible': Eval('grouping') != 'invoice',
            'required': Eval('grouping') == 'invoice'
        })
    periods = fields.Many2Many('account.period', None, None, 'Periods',
        required=True)

    @fields.depends('grouping')
    def on_change_grouping(self):
        if self.grouping == 'tax':
            self.ordering = None

    @fields.depends('type', methods=['on_change_grouping'])
    def on_change_type(self):
        if self.type == 'both':
            self.grouping = 'tax'
            self.on_change_grouping()


class PrintVatReturn(Wizard):
    """Print Vat Return"""
    __name__ = 'account.tax.vat_return.print'

    start = StateView('account.tax.vat_return.print.start',
        'account_smart_reports.vat_return_print_start_view_form',
        [Button('Cancel', 'end', 'tryton-cancel'),
         Button('Print', 'print_', 'tryton-print', default=True)])

    print_ = StateReport('account.tax.vat_return')

    def default_start(self, fields):
        return {
            'grouping': 'invoice',
            'ordering': 'invoice',
            'type': 'charged',
        }

    def do_print_(self, action):
        pool = Pool()
        TaxCode = pool.get('account.tax.code')

        types = (['charged', 'deducted']
            if self.start.type == 'both' else [self.start.type])
        tax_codes = TaxCode.search([
            ('vat_return', 'in', types)])

        return action, {
            'ids': list(map(int, tax_codes)),
            'periods': list(map(int, self.start.periods)),
            'grouping': self.start.grouping,
            'ordering': self.start.ordering,
        }


class VatReturn(CSVReport):
    """Vat Return"""
    __name__ = 'account.tax.vat_return'

    @classmethod
    def execute(cls, ids, data):
        pool = Pool()
        ActionReport = pool.get('ir.action.report')

        action_id = data.get('action_id')
        if action_id is None:
            action_reports = ActionReport.search([
                    ('report_name', '=', cls.__name__)
                    ])
            assert action_reports, '%s not found' % cls
            action_report = action_reports[0]
        else:
            action_report = ActionReport(action_id)

        oext, content, direct_print, _ = super().execute(ids, data)
        return (oext, content, direct_print, action_report.name)

    @classmethod
    def get_rows(cls, records, **kwargs):
        method = getattr(cls, 'get_grouping_%s_rows' % kwargs['grouping'])
        return method(records, **kwargs)

    @classmethod
    def get_grouping_tax_rows(cls, records, **kwargs):
        pool = Pool()
        Company = pool.get('company.company')
        TaxCode = pool.get('account.tax.code')
        TaxLine = pool.get('account.tax.line')
        Lang = pool.get('ir.lang')

        data = []
        vat_return_values = {}
        kwargs = kwargs.copy()
        company = Company(Transaction().context['company'])
        kwargs['digits'] = company.currency and company.currency.digits or 2
        kwargs['lang'] = company.party.lang or Lang.get()
        lang, digits = kwargs['lang'], kwargs['digits']
        header = [
            cls._get_translation(TaxCode, 'name'),
            cls._get_translation(TaxLine, 'type', selection=True,
                value='base'),
            cls._get_translation(TaxLine, 'type', selection=True, value='tax'),
            '%s+%s' % (
                cls._get_translation(TaxLine, 'type', selection=True,
                    value='base'),
                cls._get_translation(TaxLine, 'type', selection=True,
                    value='tax'))
        ]

        records = sorted(records, key=lambda r: r.vat_return, reverse=True)
        for vat_return, grouped_records in groupby(records,
                key=lambda r: r.vat_return):
            grouped_records = list(grouped_records)
            # Vat return header
            data.append([cls._get_translation(TaxCode, 'vat_return',
                selection=True, value=vat_return)])
            # Tax lines header
            data.append(header)
            # Tax lines
            tax_lines = cls.get_tax_lines(grouped_records, **kwargs)
            if tax_lines:
                data.extend(cls.get_tax_lines_values(
                    vat_return, tax_lines, **kwargs))
            # Total
            total_amount, base_amount, tax_amount = cls.get_total_tax_amount(
                vat_return, tax_lines)
            vat_return_values[vat_return] = tax_amount or 0
            data.append([
                'Total',
                cls.format_number(base_amount or 0, lang, digits=digits),
                cls.format_number(tax_amount or 0, lang, digits=digits),
                cls.format_number(total_amount or 0, lang, digits=digits),
            ])
            # Empty row
            data.append([])

        # Differences between vat return
        if len(vat_return_values) > 1:
            data.append([
                cls._get_report_translation('Difference', lang),
                '',
                cls.format_number(vat_return_values['deducted']
                    - vat_return_values['charged'], lang, digits=digits),
                ''])
        return data

    @classmethod
    def get_grouping_invoice_rows(cls, records, **kwargs):
        pool = Pool()
        Company = pool.get('company.company')
        Move = pool.get('account.move')
        Invoice = pool.get('account.invoice')
        TaxCode = pool.get('account.tax.code')
        TaxLine = pool.get('account.tax.line')
        Lang = pool.get('ir.lang')

        data = []
        kwargs = kwargs.copy()
        company = Company(Transaction().context['company'])
        kwargs['digits'] = company.currency and company.currency.digits or 2
        kwargs['lang'] = company.party.lang or Lang.get()
        header = [
            cls._get_translation(TaxLine, 'tax'),
            cls._get_translation(Move, 'date'),
            cls._get_translation(Invoice, 'account'),
            cls._get_translation(Invoice, 'number'),
            cls._get_translation(Invoice, 'reference'),
            cls._get_translation(Invoice, 'invoice_date'),
            cls._get_translation(Invoice, 'party'),
            cls._get_translation(Invoice, 'party_tax_identifier'),
            cls._get_translation(TaxLine, 'type', selection=True, value='base'),
            cls._get_translation(TaxLine, 'type', selection=True, value='tax'),
            '%s+%s' % (
                cls._get_translation(TaxLine, 'type', selection=True,
                    value='base'),
                cls._get_translation(TaxLine, 'type', selection=True,
                    value='tax')),
            cls._get_report_translation('Invoice amount', kwargs['lang']),
        ]

        records = sorted(records, key=lambda r: r.vat_return, reverse=True)
        for vat_return, grouped_records in groupby(records,
                key=lambda r: r.vat_return):
            grouped_records = list(grouped_records)
            # Vat return header
            data.append([cls._get_translation(TaxCode, 'vat_return',
                selection=True, value=vat_return)])
            # Tax lines header
            data.append(header)
            # Tax lines
            tax_lines = cls.get_tax_lines(grouped_records, **kwargs)
            if not tax_lines:
                continue

            data.extend(cls.get_tax_lines_values(
                vat_return, tax_lines, **kwargs))

            # Total
            total_amount, base_amount, tax_amount = cls.get_total_tax_amount(
                vat_return, tax_lines)
            data.append(['Total'] + ([''] * 7) + [
                cls.format_number(base_amount or 0, kwargs['lang'],
                    digits=kwargs['digits']),
                cls.format_number(tax_amount or 0, kwargs['lang'],
                    digits=kwargs['digits']),
                cls.format_number(total_amount or 0, kwargs['lang'],
                    digits=kwargs['digits']),
                ''
            ])
        return data

    @classmethod
    def get_tax_lines(cls, records, **kwargs):
        pool = Pool()
        Tax = pool.get('account.tax')
        TaxLine = pool.get('account.tax.line')

        periods = kwargs.get('periods', None)
        with Transaction().set_context(periods=periods):
            domain = [Tax._amount_domain()]
            subdomain = ['OR', ]
            for record in records:
                if record.lines:
                    subdomain.append(['OR']
                        + [l._line_domain for l in record.lines])
            domain.append(subdomain)
            return TaxLine.search(domain)

    @classmethod
    def get_tax_lines_query(cls, vat_return, tax_lines, grouping,
            ordering=None):
        pool = Pool()
        TaxLine = pool.get('account.tax.line')
        Tax = pool.get('account.tax')
        TaxGroup = pool.get('account.tax.group')
        MoveLine = pool.get('account.move.line')
        Move = pool.get('account.move')
        Invoice = pool.get('account.invoice')

        tax = Tax.__table__()
        tax_line = TaxLine.__table__()
        tax_group = TaxGroup.__table__()
        move_line = MoveLine.__table__()
        move = Move.__table__()
        invoice = Invoice.__table__()

        if vat_return == 'charged':
            amount_condition = (
                Case((
                        ~tax_group.kind.in_(['sale', 'both', None])
                        & (tax_line.type == 'tax'),
                    Literal(-1)), else_=Literal(1))
                * tax_line.amount
                * Case((
                        (tax_line.type == 'tax'),
                        tax.rate / Abs(tax.rate)
                    ), else_=Literal(1)))
        else:
            amount_condition = (
                Case((
                    ~tax_group.kind.in_(['purchase', 'both', None]),
                    Literal(-1)), else_=Literal(1))
                * tax_line.amount
                * Case((
                        (tax_line.type == 'tax'),
                        tax.rate / Abs(tax.rate)
                    ), else_=Literal(1)))

        columns = [Sum(amount_condition).as_('total_amount')]
        groupby = {
            None: [],
            'tax': [
                tax_line.tax,
                tax.name],
            'invoice': [
                tax_line.tax,
                tax.name,
                invoice.id,
                move.date,
            ]
        }
        group_columns = list(groupby[grouping])
        orderby = {
            None: None,
            'tax': tax.name,
            'invoice': [
                move.date if ordering == 'date' else invoice.number,
                invoice.number
            ]
        }
        for name, clause in [
                ('base_amount', (tax_line.type == 'base')),
                ('tax_amount', (tax_line.type == 'tax'))]:
            if backend.name == 'postgresql':
                columns.append(Sum(amount_condition, filter_=clause).as_(name))
            else:
                columns.append(Sum(Case([clause, amount_condition])).as_(name))

        query = tax_line.join(tax, condition=(tax_line.tax == tax.id)
            ).join(tax_group, 'LEFT', condition=(tax_group.id == tax.group)
            ).join(move_line, condition=(move_line.id == tax_line.move_line))
        if grouping == 'invoice':
            group_columns[-2] = group_columns[-2].as_('invoice')
            query = query.join(move, condition=(move.id == move_line.move)
                ).join(invoice, condition=(invoice.move == move.id)
            )
        query = query.select(
            *(group_columns + columns),
            where=reduce_ids(tax_line.id, tax_lines),
            group_by=groupby[grouping],
            order_by=orderby[grouping])
        return query

    @classmethod
    def get_tax_lines_values(cls, vat_return, tax_lines, **kwargs):
        pool = Pool()
        Invoice = pool.get('account.invoice')

        cursor = Transaction().connection.cursor()

        lang, digits = kwargs['lang'], kwargs['digits']
        values = []
        if kwargs['grouping'] == 'invoice':

            cursor.execute(*cls.get_tax_lines_query(vat_return, tax_lines,
                grouping='invoice', ordering=kwargs['ordering']))
            fetchall = list(cursor_dict(cursor))
            id2invoices = {i.id: i for i in Invoice.browse(
                r['invoice'] for r in fetchall if r['invoice'])}
            invoice_amounts = {invoice.id: sum(abs(line.debit - line.credit)
                    for line in invoice.move.lines
                    if line.account == invoice.account
                ) for invoice in id2invoices.values()
            }

            for row in fetchall:
                invoice = id2invoices[row['invoice']]
                invoice_tax = [l for l in invoice.move.lines
                    if l.tax_lines
                    and any(t.tax.id == row['tax'] and t.type == 'tax'
                        for t in l.tax_lines)
                ]
                tax_account = None
                if invoice_tax:
                    tax_account = invoice_tax[0].account
                else:
                    # get account from invoice taxes
                    invoice_tax = [t for t in invoice.taxes
                        if t.tax.id == row['tax']]
                    if invoice_tax:
                        tax_account = invoice_tax[0].account
                tax_identifier = (invoice.party_tax_identifier
                    or invoice.party.tax_identifier
                )
                if not tax_identifier and invoice.party.identifiers:
                    for identifier in invoice.party.identifiers:
                        if not identifier.type:
                            tax_identifier = identifier
                            break
                values.append([
                    row['name'],
                    row['date'],
                    tax_account and tax_account.code or '',
                    invoice.number,
                    invoice.reference,
                    invoice.invoice_date,
                    invoice.party.name,
                    tax_identifier and tax_identifier.code or '',
                    cls.format_number(row['base_amount'] or 0, lang,
                        digits=digits),
                    cls.format_number(row['tax_amount'] or 0, lang,
                        digits=digits),
                    cls.format_number(row['total_amount'] or 0, lang,
                        digits=digits),
                    cls.format_number(invoice_amounts[row['invoice']],
                        lang, digits=digits),
                ])

        elif kwargs['grouping'] == 'tax':
            cursor.execute(*cls.get_tax_lines_query(vat_return, tax_lines,
                grouping='tax'))
            for row in cursor.fetchall():
                tax_id, tax_name, total_amount, base_amount, tax_amount = row
                data = [
                    tax_name,
                    cls.format_number(base_amount or 0, lang, digits=digits),
                    cls.format_number(tax_amount or 0, lang, digits=digits),
                    cls.format_number(total_amount or 0, lang, digits=digits)
                ]
                values.append(data)
        return values

    @classmethod
    def get_total_tax_amount(cls, vat_return, tax_lines):
        cursor = Transaction().connection.cursor()
        cursor.execute(*cls.get_tax_lines_query(vat_return, tax_lines,
            grouping=None))
        return cursor.fetchone()
