# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import account
from . import tax


def register():
    Pool.register(
        tax.TaxCode,
        tax.TaxCodeTemplate,
        tax.PrintVatReturnStart,
        module='account_smart_reports', type_='model')
    Pool.register(
        tax.PrintVatReturn,
        module='account_smart_reports', type_='wizard')
    Pool.register(
        account.GeneralLedgerList,
        tax.VatReturn,
        account.GeneralLedger,
        module='account_smart_reports', type_='report')
