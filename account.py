# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.modules.ir_report_csv.model import CSVReport


class GeneralLedgerList(CSVReport):
    __name__ = 'account.general_ledger.list'

    @classmethod
    def get_lang(cls, record):
        pool = Pool()
        Company = pool.get('company.company')

        company_id = Transaction().context.get('company')
        if company_id:
            return Company(company_id).party.lang

    @classmethod
    def get_rows(cls, records, **kwargs):
        pool = Pool()
        AccountLedger = pool.get('account.general_ledger.account')

        data = []
        balance_headers = {
            'start': cls._get_translation(AccountLedger, 'start_balance'),
            'end': cls._get_translation(AccountLedger, 'end_balance')
        }
        for record in records:
            lang = cls.get_lang(record)
            digits = record.currency.digits or 2
            # account header
            data.append([record.rec_name])
            # move line header
            _fields = cls._get_general_ledger_list_fields(record)
            data.append(cls.get_general_ledger_list_fields(record))
            # start balance
            data.append(cls.get_start_balance_line(record,
                balance_headers['start'], lang, digits))
            # move lines
            for line in cls.get_general_ledger_lines(record):
                data.append(cls.get_general_ledger_list_values(record, line))
            # total
            total_row = [
                'Total',
                cls.format_number(record.debit, lang, digits=digits),
                cls.format_number(record.credit, lang, digits=digits),
                ''
            ]
            total_row = ([''] * (len(_fields) - len(total_row))) + total_row
            data.append(total_row)
            # end balance
            end_balance_row = [''] * len(_fields)
            end_balance_row[_fields.index('description')] = (
                balance_headers['end'])
            end_balance_row[_fields.index('balance')] = cls.format_number(
                record.end_balance, lang, digits=digits)
            data.append(end_balance_row)
            # empty row
            data.append([])
        return data

    @classmethod
    def get_start_balance_line(cls, record, description, lang, digits):
        _fields = cls._get_general_ledger_list_fields(record)
        start_balance_row = [''] * len(_fields)
        start_balance_row[_fields.index('description')] = description
        start_balance_row[_fields.index('balance')] = cls.format_number(
            record.start_balance, lang, digits=digits)
        return start_balance_row

    @classmethod
    def get_general_ledger_lines(cls, record):
        pool = Pool()
        LedgerLine = pool.get('account.general_ledger.line')

        if record.__name__ == 'account.general_ledger.account':
            return record.lines
        context = Transaction().context
        domain = [
            ('account.account', '=', record.account),
            ('party', '=', record.party),
        ]
        if context.get('from_date', None):
            domain.append(('date', '>=', context['from_date']))
        if context.get('to_date', None):
            domain.append(('date', '<=', context['to_date']))
        with Transaction().set_context(party_cumulate=True):
            lines = LedgerLine.search(domain)
        return lines

    @classmethod
    def get_general_ledger_list_fields(cls, record):
        pool = Pool()
        LedgerLine = pool.get('account.general_ledger.line')

        data = []
        for _field in cls._get_general_ledger_list_fields(record):
            _field = _field.split('.')
            values = []
            for x, f in enumerate(_field):
                value = cls._get_translation(LedgerLine, f)
                if x > 0:
                    value = '(%s)' % (value or '')
                elif value is None:
                    value, = _field
                values.append(value or '')
            data.append(' '.join(values))
        return data

    @classmethod
    def _get_general_ledger_list_fields(cls, record):
        values = ['date', 'move', 'party', 'description', 'origin',
            'origin.rec_name', 'debit', 'credit', 'balance']
        if not cls.party_required(record):
            values.remove('party')
        return values

    @classmethod
    def get_general_ledger_list_values(cls, record, line):
        pool = Pool()
        LedgerLine = pool.get('account.general_ledger.line')

        model_name = None
        if line.origin:
            model_name = getattr(LedgerLine._fields['origin'],
                'translated')().__get__(line, LedgerLine)

        digits = line.currency.digits or 2
        lang = cls.get_lang(record)
        values = [
            line.date and cls.format_date(line.date, lang) or '',
            line.move and line.move.rec_name or '',
            line.description or '',
            model_name or '',
            line.origin and line.origin.rec_name or '',
            cls.format_number(line.debit, lang, digits=digits),
            cls.format_number(line.credit, lang, digits=digits),
            cls.format_number(line.balance, lang, digits=digits),
        ]
        if cls.party_required(record):
            values.insert(2, line.party and line.party.rec_name or '')
        return values

    @classmethod
    def party_required(cls, record):
        if not record:
            return False
        return (record.account.party_required
            and record.__name__ != 'account.general_ledger.account.party')


class GeneralLedger(metaclass=PoolMeta):
    __name__ = 'account.general_ledger'

    @classmethod
    def get_context(cls, records, header, data):
        context = Transaction().context
        report_context = super().get_context(records, header, data)
        if not context.get('fiscalyear'):
            report_context['fiscalyear'] = None

        return report_context
